import React, { Component } from 'react';

import OffersPage from './components/OffersPage/OffersPage.jsx';

class App extends Component {
  render() {
    return (
      <OffersPage />
    );
  }
}

export default App;
