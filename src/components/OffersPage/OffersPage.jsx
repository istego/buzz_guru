import React, { Component } from 'react';

import OffersCard from '../OffersCard/OffersCard.jsx';
import OffersPopup from '../OffersPopup/OffersPopup.jsx';

import stubData from '../../data.json';
import offerPopupData from '../../offerPopup.json';

import classNames from 'classnames/bind';
import styles from './OffersPage.pcss';

const cx = classNames.bind(styles);

const userId = 1;
const userRole = 'advertiser';

export default class OffersPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offers: [],
      ownItemsFilter: false,
      isPopupOpen: false,
      popupData: null,
    };

    this.handleTabClick = this.handleTabClick.bind(this);
    this.openPopup = this.openPopup.bind(this);
    this.closePopup = this.closePopup.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        offers: stubData,
      })
    }, 100);
  }

  handleTabClick(value) {
    this.setState({
      ownItemsFilter: value,
    });
  }

  openPopup(id) {
    setTimeout(() => {
      this.setState({
        popupData: offerPopupData,
        isPopupOpen: true,
      });
    }, 100);
  }

  closePopup() {
    this.setState({
      isPopupOpen: false,
      popupData: null,
    });
  }

  render() {
    const {
      offers,
      ownItemsFilter,
      isPopupOpen,
      popupData,
    } = this.state;

    return (
      <div className={cx('wrapper')}>
        <div className={cx('container', 'header')}>
          <div className={cx('headerItem', 'tabsWrap')}>
            <div className={cx('tabs')}>
              <span className={cx('tabsItem', {active:!ownItemsFilter})} onClick={() => this.handleTabClick(false)}>Все предложения</span>
              <span className={cx('tabsItem', {active:ownItemsFilter})} onClick={() => this.handleTabClick(true)}>Мои</span>
            </div>
          </div>
          <div className={cx('headerItem', 'buttonWrap')}>
            <button className={cx('newOfferButton')}>Новое предложение</button>
          </div>
        </div>
        <div className={cx('container', 'catalog')}>
          {
            offers.map((item) => {
              if (!ownItemsFilter) {
                return <OffersCard key={item.id} {...item} currentUserId={userId} openPopup={this.openPopup} />;
              }
              return item.userId === userId ? <OffersCard key={item.id} {...item} openPopup={this.openPopup} currentUserId={userId} /> : null;
            })
          }
        </div>
        {
          isPopupOpen && <OffersPopup {...popupData} role={userRole} currentUserId={userId} closePopup={this.closePopup} />
        }
      </div>
    );
  }
}
