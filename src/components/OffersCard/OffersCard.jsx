import React from 'react';
import PropTypes from 'prop-types';

import languages from '../../lang.json';
import statuses from '../../statuses.json';

import classNames from 'classnames/bind';
import styles from './OffersCard.pcss';

const cx = classNames.bind(styles);

const OffersCard = (props) => {
  const {
    id,
    views,
    people,
    price,
    date,
    imageSrc,
    title,
    description,
    logoSrc,
    tags,
    status,
    userId,
    currentUserId,
    isPrivate,
  } = props;

  const editOffer = (id, e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log(id);
  };

  const isOwnOffer = currentUserId === userId;
  const titleMaxLength = 36;
  const titleCut = title.length < titleMaxLength ? title : `${title.slice(0, titleMaxLength)}…`;

  const descriptionMaxLength = 84;
  const descriptionCut = description.length < descriptionMaxLength ? description : `${description.slice(0, descriptionMaxLength)}…`;

  const language = languages[props.language];

  return (
    <div className={cx('wrapper', {ownOffer: isOwnOffer})} onClick={() => props.openPopup(id)}>
      <div className={cx('imageWrap')} style={{backgroundImage: `url(${imageSrc})`}}>
        <div className={cx('infoCaption')}>
          <div className={cx('infoCaptionItem')}>
            <svg className={cx('infoCaptionIcon')} xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 16 15">
              <g fill="none" fillRule="evenodd">
                <path d="M-1-1h18v18H-1z"/>
                <path fill="#FFF" fillRule="nonzero" d="M8.633 9.803L6.785 7.92l.022-.022A13.25 13.25 0 0 0 9.505 3h2.131V1.5h-5.09V0H5.09v1.5H0V3h8.124a11.909 11.909 0 0 1-2.306 4.012A11.752 11.752 0 0 1 4.138 4.5H2.684A13.22 13.22 0 0 0 4.85 7.92l-3.702 3.765 1.033 1.065L5.818 9l2.262 2.332.553-1.53zM12.727 6h-1.454L8 15h1.455l.814-2.25h3.455l.821 2.25H16l-3.273-9zm-1.905 5.25L12 8.002l1.178 3.248h-2.356z"/>
              </g>
            </svg>
            <span className={cx('infoCaptionText')}>{language}</span>
          </div>
          <div className={cx('infoCaptionItem')}>
            <svg className={cx('infoCaptionIcon')} xmlns="http://www.w3.org/2000/svg" width="16" height="10" viewBox="0 0 16 10">
              <g fill="none" fillRule="evenodd">
                <path d="M-1-3h18v18H-1z"/>
                <path fill="#FFF" fillRule="nonzero" d="M8 0C4.364 0 1.258 2.073 0 5c1.258 2.927 4.364 5 8 5s6.742-2.073 8-5c-1.258-2.927-4.364-5-8-5zm0 9C5.792 9 4 7.208 4 5s1.792-4 4-4 4 1.792 4 4-1.792 4-4 4zm0-6c-1.107 0-2 .893-2 2s.893 2 2 2 2-.893 2-2-.893-2-2-2z"/>
              </g>
            </svg>
            <span className={cx('infoCaptionText')}>{views}</span>
          </div>
          <div className={cx('infoCaptionItem')}>
            <svg className={cx('infoCaptionIcon')} xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
              <g fill="none" fillRule="evenodd">
                <path d="M-2-2h18v18H-2z"/>
                <path fill="#FFF" fillRule="nonzero" d="M5 7a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm4 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2zM7 0C3.136 0 0 3.136 0 7s3.136 7 7 7 7-3.136 7-7-3.136-7-7-7zm0 13c-3.308 0-6-2.775-6-6.184 0-.224.015-.448.038-.665C2.808 5.34 4.21 4.848 4.944 3c1.357 1.979 3.592 2.27 6.12 2.27.585 0 1.148-.07 1.688-.201.157.549.247 1.136.247 1.747C13 10.225 10.307 13 7 13z"/>
              </g>
            </svg>
            <span className={cx('infoCaptionText')}>{people}</span>
          </div>
        </div>
        {
          isOwnOffer && status && status !== 'default' && statuses[status] && (
            <div className={cx('status', `status_${status}`)}>
              <div className={cx('statusText')}>{statuses[status].title}</div>
            </div>
          )
        }
        <div className={cx('datePriceWrap')}>
          {
            isPrivate && <span className={cx('privateOffer')}>Приватное предложение</span>
          }
          <div className={cx('price')}>{price}</div>
          <div className={cx('date')}>
            {
              date
                ? new Date(date).toLocaleString("ru", {day: 'numeric', month: 'numeric', year: 'numeric'})
                : 'Свободная дата'
            }
          </div>
        </div>
      </div>
      <div className={cx('contentBlock')}>
        <div className={cx('contentText')}>
          <h3 className={cx('title')}>{titleCut}</h3>
          <p className={cx('description')}>{descriptionCut}</p>
          <div className={cx('logo')}>
            <img className={cx('logoImg')} src={logoSrc} alt="" />
          </div>
        </div>
        {
          isOwnOffer && status && <button className={cx('editButton')} onClick={(e) => editOffer(id, e)}>Редактировать</button>
        }
        <div className={cx('tags')}>
          {
            tags.map((tag, i) => {
              return i !== tags.length - 1 ? <span key={i}>{tag}, </span> : <span key={i}>{tag}</span>;
            })
          }
        </div>
      </div>
    </div>
  );
};

OffersCard.propTypes = {
  id: PropTypes.any.isRequired,
  views: PropTypes.number,
  people: PropTypes.number,
  price: PropTypes.string.isRequired,
  date: PropTypes.number,
  imageSrc: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  logoSrc: PropTypes.string.isRequired,
  tags: PropTypes.array,
  status: PropTypes.string.isRequired,
  userId: PropTypes.any.isRequired,
  currentUserId: PropTypes.any.isRequired,
  isPrivate: PropTypes.bool,
};

export default OffersCard;
