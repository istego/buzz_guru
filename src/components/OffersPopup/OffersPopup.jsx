import React from 'react';
import PropTypes from 'prop-types';

import statuses from '../../statuses.json';

import classNames from 'classnames/bind';
import styles from './OffersPopup.pcss';

const cx = classNames.bind(styles);

const OffersPopup = (props) => {
  const {
    status,
    author,
    img,
    title,
    parameters,
    isPrivate,
    description,
    response,
    role,
    currentUserId,
    files,
  } = props;

  const isOwnOffer = currentUserId === author.id;
  const isAdvertiser = role === 'advertiser';
  const disabled = status === 'moderated' || status === 'closed';
  const hasResponses = !response || !response.length;

  const inviteBlogers = () => {
    console.log('invite blogers func');
  };

  const renderActionButton = () => {
    switch (role) {
      case 'advertiser':
        if (currentUserId) {
          if (hasResponses) {
            return <button className={cx('actionButton', {actionButtonDisabled: disabled})} disabled={disabled}>Редактировать</button>;
          }
          return <button className={cx('actionButton')}>К сделке</button>;
        }
        return null;
      case 'bloger':
        if (hasResponses) {
          return <button className={cx('actionButton')}>Откликнуться</button>;
        }
        return <button className={cx('actionButton')}>Отказаться</button>;
      default:
        return null;
    }
  };

  const renderStatusMessage = () => {
    if (isOwnOffer && isAdvertiser && status !== 'default') {
      return (
        <div className={cx('popupStatus', `popupStatus_${status}`)}>{statuses[status].title}</div>
      );
    }
    return null;
  };

  const renderResponses = () => {
    if (isOwnOffer && isAdvertiser) {
      if (hasResponses) {
        return (
          <div className={cx('inviteBlogers')}>
            <div className={cx('inviteBlogersCaption')}>Вы еще не приглашали блогеров</div>
            <div className={cx('inviteBlogersButton')} onClick={inviteBlogers}>
              <div className={cx('inviteBlogersIcon')}>
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                  <path fill="#4B86C6" fillRule="nonzero" d="M11 5H9v4H5v2h4v4h2v-4h4V9h-4z"/>
                </svg>
              </div>
              <span className={cx('inviteBlogersText')}>Пригласить блогеров</span>
            </div>
          </div>
        );
      } else {
        const responses = response.length > 4 ? response.slice(0, 4) : response;
        return (
          <div className={cx('blogers')}>
            <div className={cx('blogersCaption')}>Отклики</div>
            <div className={cx('blogersRow')}>
              {
                responses.map((item, index) => <div key={index} className={cx('blogersItem')}></div>)
              }
              {
                response.length > 4 && <div className={cx('blogersItem', 'blogersItemLast')}>+{response.length - 4}</div>
              }
            </div>
          </div>
        );
      }
    }
    return null;
  };

  return (
    <div className={cx('wrapper')}>
      <div className={cx('overlay')} onClick={props.closePopup}></div>
      <div className={cx('popup')}>
        <div className={cx('closeButton')} onClick={props.closePopup}>
          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12">
            <g fill="none" fillRule="evenodd">
              <path d="M-6-6h24v24H-6z"/>
              <path fill="#9B9B9B" fillRule="nonzero" d="M11.586 1.824l-1.41-1.41L6 4.59 1.824.414l-1.41 1.41L4.59 6 .414 10.176l1.41 1.41L6 7.41l4.176 4.176 1.41-1.41L7.41 6z"/>
            </g>
          </svg>
        </div>
        <div className={cx('popupContainer')}>
          <div className={cx('sidebar')}>
            <div className={cx('image')} style={{backgroundImage:`url(${img})`}}>
              {
                renderStatusMessage()
              }
            </div>
            {
              status === "rejected" && (
                <div className={cx('rejectedDescription')}>Ваше предложение не удволетворяет потребности сервиса. Измените описание товара. В случае затруднения свяжитесь с менеджером.</div>
              )
            }
            <div className={cx('infoBlock')}>
              {
                parameters.terms ? (
                  <div className={cx('infoItem')}>
                    <div className={cx('infoLabel')}>Сроки публикации</div>
                    <div className={cx('infoValue')}>
                      <div className={cx('infoValueIcon')}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="21" viewBox="0 0 18 21">
                          <g fill="none" fillRule="evenodd">
                            <path d="M-3-1h24v24H-3z"/>
                            <path fill="#CFDFF0" fillRule="nonzero" d="M12 0H6v2h6V0zM8 13h2V7H8v6zm8.03-6.61l1.42-1.42c-.43-.51-.9-.99-1.41-1.41l-1.42 1.42A8.962 8.962 0 0 0 9 3a9 9 0 0 0-9 9c0 4.97 4.02 9 9 9a8.994 8.994 0 0 0 7.03-14.61zM9 19c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"/>
                          </g>
                        </svg>
                      </div>
                      <span className={cx('infoValueText')}>{parameters.terms}</span>
                    </div>
                  </div>
                ) : null
              }
              {
                parameters.reward ? (
                  <div className={cx('infoItem')}>
                    <div className={cx('infoLabel')}>Вознаграждение</div>
                    <div className={cx('infoValue')}>
                      <div className={cx('infoValueIcon')}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="18" viewBox="0 0 11 18">
                          <g fill="none" fillRule="evenodd">
                            <path d="M-6-3h24v24H-6z"/>
                            <path fill="#CFDFF0" fillRule="nonzero" d="M5.8 7.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.78 0 2.44.85 2.5 2.1h2.21C10.14 4.28 9.09 2.7 7 2.19V0H4v2.16C2.06 2.58.5 3.84.5 5.77c0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-2.06 0-2.87-.92-2.98-2.1H.32c.12 2.19 1.76 3.42 3.68 3.83V18h3v-2.15c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z"/>
                          </g>
                        </svg>
                      </div>
                      <span className={cx('infoValueText')}>{parameters.reward}</span>
                    </div>
                  </div>
                ) : null
              }
              {
                parameters.site ? (
                  <div className={cx('infoItem')}>
                    <div className={cx('infoLabel')}>Сайт</div>
                    <div className={cx('infoValue')}>
                      <div className={cx('infoValueIcon')}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                          <g fill="none" fillRule="evenodd">
                            <path d="M-2-2h24v24H-2z"/>
                            <path fill="#CFDFF0" fillRule="nonzero" d="M10 0a10 10 0 0 1 10 10c0 5.523-4.477 10-10 10a10 10 0 1 1 0-20zm0 2a8 8 0 0 0-8 8c0 2.09.8 4 2.11 5.41l3.77-7.53 7.53-3.77A7.931 7.931 0 0 0 10 2zm0 16a8 8 0 0 0 8-8c0-2.09-.8-4-2.11-5.41l-3.77 7.53-7.53 3.77A7.931 7.931 0 0 0 10 18zm0-8l-.77-.77L7.7 12.3l3.07-1.53L10 10zm0 5.5h1V17h-1v-1.5zm3.88-1.61l.71-.71 1.06 1.06-.71.71-1.06-1.06zM15.5 10V9H17v1h-1.5zM10 4.5H9V3h1v1.5zM6.12 6.11l-.71.71-1.06-1.06.71-.71 1.06 1.06zM4.5 10v1H3v-1h1.5z"/>
                          </g>
                        </svg>
                      </div>
                      <a href={parameters.site} target="_blank" className={cx('infoValueLink')}>Открыть</a>
                    </div>
                  </div>
                ) : null
              }
            </div>
            { renderActionButton() }
          </div>
          <div className={cx('content')}>
            <header className={cx('header')}>
              <div className={cx('authorInfo')}>
                <div className={cx('authorLogo')}>
                  <img className={cx('authorLogoImg')} src={author.img} alt="" />
                </div>
                <span className={cx('authorName')}>{author.name}</span>
              </div>
              <h2 className={cx('title')}>{title}</h2>
              {
                isPrivate && <span className={cx('privateOffer')}>Приватное предложение</span>
              }
              {
                renderResponses()
              }
            </header>
            {
              description && description.length ? (
                description.map((item, index) => (
                  <div key={index} className={cx('descriptionBlock')}>
                    <div className={cx('descriptionLabel')}>{item.subtitle}</div>
                    <p className={cx('descriptionText')}>{item.text}</p>
                  </div>
                ))
              ) : null
            }
            {
              files && files.number > 0 && (
                <div className={cx('files')}>
                  <div className={cx('filesCaption')}>
                    <a href={files.link} className={cx('filesCaptionLink')}>Приложенные файлы: {files.number}</a>
                    <span className={cx('filesSize')}>{files.size}</span>
                  </div>
                  <a href={files.link} className={cx('filesDownload')}>Скачать</a>
                </div>
              )
            }
          </div>
        </div>
      </div>
    </div>
  );
};

OffersPopup.propTypes = {
  status: PropTypes.string.isRequired,
  author: PropTypes.object.isRequired,
  img: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  parameters: PropTypes.object.isRequired,
  isPrivate: PropTypes.bool,
  description: PropTypes.array,
  response: PropTypes.array.isRequired,
  role: PropTypes.string.isRequired,
  currentUserId: PropTypes.any.isRequired,
  files: PropTypes.object,
};

export default OffersPopup;
